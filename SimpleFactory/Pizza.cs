﻿using System;

namespace SimpleFactory
{
    public abstract class Pizza
    {
        public abstract void Prepare();

        public  void Bake()
        {
            Console.WriteLine("baking pizza");
        }

        public void Cut()
        {
            Console.WriteLine("cutting pizza");
        }

        public void Box()
        {
            Console.WriteLine("boxing pizza");
        }
    }

    public class CheesePizza : Pizza
    {
        public override void Prepare()
        {
            Console.WriteLine("prepare cheese pizza");
        }
    }

    public class MushroomPizza : Pizza
    {
        public override void Prepare()
        {
            Console.WriteLine("prepare mushroom pizza");
        }
    }

    public class ChickenPizza : Pizza
    {
        public override void Prepare()
        {
            Console.WriteLine("prepare mushroom pizza");
        }
    }
}
