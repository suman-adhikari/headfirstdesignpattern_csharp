﻿using System;

namespace SimpleFactory
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("SIMPLE FACTORY PATTERN");

            PizzaStore store = new PizzaStore();
            store.OrderPizza("mushroom");
        }
    }
}
