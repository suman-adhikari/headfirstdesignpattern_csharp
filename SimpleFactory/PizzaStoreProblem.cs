﻿
namespace SimpleFactory
{
    public class PizzaStoreProblem
    {
        private Pizza pizza;

        //The problem with this code is that, if new type of pizza has to be added/removed, this code needs to be modified.
        //BUT remember the code should be closed for modification
        //SOLUTION, Also remember, seperate what varies(changes). So we will keep this object creating login to a seperate class called factory
        // Factory is only responsible for creating concrete pizza
        //This way, we can add or delete pizza without changing the pizzaStore code, AND that simplly is the SIMPLE FACTORY PATTERN
        //Also since this pizza creating code ie pizza factory is kept seperated,
        // other clients such as RoadWayPizzaStore, PizzaManiaStore can also use the pizza factor to get their pizza, AWESOME.

        public void OrderPizza(string type)
        {
            if (type == "chicken") 
                pizza = new ChickenPizza();
            else if(type == "mushroom") 
                pizza = new MushroomPizza();
            else 
                pizza = new CheesePizza();

            pizza.Prepare();
            pizza.Bake();
            pizza.Cut();
            pizza.Box();
        }

    }
}
