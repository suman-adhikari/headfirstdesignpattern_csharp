﻿using System;
using System.Collections.Generic;
using System.Text;

namespace SimpleFactory
{
    public class SimplePizzaFactory
    {  
        public static Pizza CreatePizza(string type)
        {
            Pizza pizza;

            if (type == "chicken")
                pizza = new ChickenPizza();
            else if (type == "mushroom")
                pizza = new MushroomPizza();
            else
                pizza = new CheesePizza();

            return pizza;
        }
    }
}
