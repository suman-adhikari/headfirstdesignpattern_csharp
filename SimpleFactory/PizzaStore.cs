﻿
namespace SimpleFactory
{
    public class PizzaStore
    {
       // private Pizza pizza;
        public void OrderPizza(string type)
        {
            Pizza pizza = SimplePizzaFactory.CreatePizza(type);
            pizza.Prepare();
            pizza.Bake();
            pizza.Cut();
            pizza.Box();
        }

    }
}
