﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Facade
{
    public class SubSystem
    {
    }

    public class Light
    {
        public void LightOn()
        {
            Console.WriteLine("The Light is on");
        }

        public void Off()
        {
            Console.WriteLine("Light off");
        }
    }

    public class Projector
    {
        public void ProjectorOn()
        {
            Console.WriteLine("The Projector is on");
        }

        public void Off()
        {
            Console.WriteLine("shutting down Projector");
        }
    }

    public class Laptop
    {
        public void LaptopOn()
        {
            Console.WriteLine("The Laptop is on");
        }

        public void ConnectToProjector()
        {
            Console.WriteLine("The Laptop is connected to projector");
        }

        public void DisconnectProjector()
        {
            Console.WriteLine("The Laptop is connected to projector");
        }

        public void Play(string movie)
        {
            Console.WriteLine($"Playing {movie}");
        }

        public void Stop()
        {
            Console.WriteLine("stopping movie");
        }
        public void Off()
        {
            Console.WriteLine("shutting down laptop");
        }
    }

    public class PopCorn
    {
        public void PopCornOn()
        {
            Console.WriteLine("The PopCorn is on");
        }

        public void PopCornReady()
        {
            Console.WriteLine("The PopCorn is ready");
        }
    }


}
