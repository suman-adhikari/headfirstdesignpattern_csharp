﻿using System;

namespace Facade
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("--Facade Pattern --");

            //WIthout Facade
            //The issue with this system is that the client is tightly coupled with the subsystem(multiple classes)
            //The change in one class of system (changed the method name or changed the parameter) or addition/removing of one entity of subsystem 
            //directly affects the clinet. Now the client won't funtion and we have to change code in client as well.
            //Also the client will be dependent on N number of concrete class. More dependency you have, more maintainance required.
            Light light = new Light();
            Projector projector = new Projector();
            Laptop laptop = new Laptop();
            PopCorn popCorn = new PopCorn();

            //start movie
            Console.WriteLine("\n Starting home theater");
            light.LightOn();
            projector.ProjectorOn();
            laptop.LaptopOn();
            laptop.ConnectToProjector();
            popCorn.PopCornOn();
            popCorn.PopCornReady();
            laptop.Play("LOR");

            //end movie
            Console.WriteLine("\n Stopping home theater");
            laptop.Stop();
            laptop.DisconnectProjector();
            laptop.Off();
            projector.Off();
            light.Off();


            //Facade way, looks much better and now clinet is dependent on only one class: The facade class. You can add/remove any number
            //of subsystem, the clinet won't know or even care. ALl that will be handled by Facade. Client now gets clean interface to get the 
            //things done and won't have to know the complexity of sub-system
            Console.WriteLine("\n The Facde way");
            FacadeClientTheater theater = new FacadeClientTheater();
            
            Console.WriteLine("\n Starting home theater");
            theater.StartHomeTheater("LIfe is Beautiful");

            Console.WriteLine("\n Stopping home theater");
            theater.StopHomeTheater();


        }
    }
}
