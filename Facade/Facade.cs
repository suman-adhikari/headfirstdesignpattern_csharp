﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Facade
{
   
    public class FacadeClientTheater
    {
        private Light light;
        private Projector projector;
        private Laptop laptop;
        private PopCorn popCorn;
        public FacadeClientTheater()
        {
            light = new Light();
            projector = new Projector();
            laptop = new Laptop();
            popCorn = new PopCorn();
        }

        public void StartHomeTheater(string movie)
        {
            light.LightOn();
            projector.ProjectorOn();
            laptop.LaptopOn();
            laptop.ConnectToProjector();
            popCorn.PopCornOn();
            popCorn.PopCornReady();
            laptop.Play(movie);
        }

        public void StopHomeTheater()
        {
            laptop.Stop();
            laptop.DisconnectProjector();
            laptop.Off();
            projector.Off();
            light.Off();
        }


    }
}
