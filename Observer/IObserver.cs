﻿
namespace Observer
{
    interface IObserver
    {

        public void Update(WeatherData data);
    }

    class HoverBoard : IObserver
    {
        IDisplay BoardDisplay = new BoardDisplay();

        public void Update(WeatherData data)
        {
            BoardDisplay.DisplayMessage(data);
        }
    }

    class OfficeBoard : IObserver
    {
        IDisplay BoardDisplay = new BoardDisplay();
        public void Update(WeatherData data)
        {
            BoardDisplay.DisplayMessage(data);
        }
    }

    class Mobile : IObserver
    {
        IDisplay mobileDisplay = new mobileDisplay();
        public void Update(WeatherData data)
        {
            mobileDisplay.DisplayMessage(data);
        }
    }
}
