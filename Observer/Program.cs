﻿using System;

namespace Observer
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Observer Pattern");

            Weather weather = new Weather();
            weather.AddObserver(new HoverBoard());
            weather.AddObserver(new OfficeBoard());
            weather.AddObserver(new Mobile());
            //weather.RemoveObserver(new OfficeBoard());

            WeatherData data = new WeatherData();
            data.temp = 17;
            data.humidity = 100;
            data.weatherPattern = "cloudy";

            weather.data = data;
            weather.NotifyObserver();

        }
    }
}
