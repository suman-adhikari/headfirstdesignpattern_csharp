﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Observer
{
    interface IDisplay
    {
        void DisplayMessage(WeatherData data);
    }

    class BoardDisplay : IDisplay
    {
        public void DisplayMessage(WeatherData data)
        {
            Console.WriteLine($"Weather Info-\nTemp : {data.temp}\nHumidity:{data.humidity}\nWeather:{data.weatherPattern}\n");
        }
    }

    class mobileDisplay : IDisplay
    {
        public void DisplayMessage(WeatherData data)
        {
            Console.WriteLine($"The weather is {data.weatherPattern}, temperature is {data.temp} degree celcius and Humidity is {data.humidity} mm\n");
        }
    }


}

