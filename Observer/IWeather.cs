﻿using System.Collections.Generic;

namespace Observer
{
    //Subject
    interface IWeather
    {
        public void AddObserver(IObserver observer);
        public void RemoveObserver(IObserver observer);
        public void NotifyObserver();
    }

    class WeatherData
    {
        public double temp;
        public string weatherPattern;
        public double humidity;

    }

    class Weather : IWeather
    {
        public WeatherData data { get; set; }

        List<IObserver> _observer = new List<IObserver>();
        public void AddObserver(IObserver observer)
        {
            _observer.Add(observer);
        }
        public void RemoveObserver(IObserver observer)
        {
            _observer.RemoveAll(x => x.ToString() == observer.ToString());
        }
        public void NotifyObserver()
        {
            foreach(var observer in _observer)
            {
                observer.Update(data);
            }
        }
    }


}
