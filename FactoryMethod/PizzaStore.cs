﻿
namespace FactoryMethodApproachOne
{
    class PizzaStore
    {
        IPizzaFactory pizzaFactory;

        public PizzaStore(IPizzaFactory pizzaFactory)
        {
            this.pizzaFactory = pizzaFactory;
        }
        public Pizza OrderPizza(string type)
        {
            Pizza pizza = pizzaFactory.createPizza(type);
            pizza.Prepare();
            pizza.Bake();
            pizza.Cut();
            pizza.Box();
            return pizza;
        }
    }
}
