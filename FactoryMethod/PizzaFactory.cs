﻿
namespace FactoryMethodApproachOne
{
    public interface IPizzaFactory
    {
        //object creating method is called factory method
        public Pizza createPizza(string type);
    }

    public class NYPizzaFactory : IPizzaFactory{
        public Pizza createPizza(string type)
        {
            Pizza pizza;
            if (type == "chicken")
                pizza = new NYChickenPizza();
            else if (type == "mushroom")
                pizza = new NYMushroomPizza();
            else
                pizza = new NYCheesePizza();
            return pizza;
        }
    }

    public class ChicagoPizzaFactory : IPizzaFactory
    {
        public Pizza createPizza(string type)
        {
            Pizza pizza;
            if (type == "chicken")
                pizza = new ChicagoChickenPizza();
            else if (type == "mushroom")
                pizza = new ChicagoMushroomPizza();
            else
                pizza = new ChicagoCheesePizza();
            return pizza;
        }
    }
}
