﻿using System;

namespace FactoryMethodApproachOne
{
    public abstract class Pizza
    {
        public abstract void Prepare();

        public void Bake()
        {
            Console.WriteLine("baking pizza");
        }

        public void Cut()
        {
            Console.WriteLine("cutting pizza");
        }

        public void Box()
        {
            Console.WriteLine("boxing pizza");
        }
    }
    //NewYork and Chicago might have different way of preparing pizza based on local customer's choise
    //NewYorkers love thin crust with little sause and cheese
    //Chicago people love tick crust with heavy sause and cheese
    //But the fracnchise wants the way to bake,cut and box(packaging box) to be thes same to keep consistency.
    public class NYCheesePizza : Pizza
    {
        public override void Prepare()
        {
            Console.WriteLine("prepare NYCheesePizza pizza");
        }
    }

    public class ChicagoCheesePizza : Pizza
    {
        public override void Prepare()
        {
            Console.WriteLine("prepare ChicagoCheesePizza pizza");
        }
    }

    public class NYMushroomPizza : Pizza
    {
        public override void Prepare()
        {
            Console.WriteLine("prepare NYMushroomPizza pizza");
        }
    }

    public class ChicagoMushroomPizza : Pizza
    {
        public override void Prepare()
        {
            Console.WriteLine("prepare ChicagoMushroomPizza pizza");
        }
    }
    public class NYChickenPizza : Pizza
    {
        public override void Prepare()
        {
            Console.WriteLine("prepare NYChickenPizza pizza");
        }
    }

    public class ChicagoChickenPizza : Pizza
    {
        public override void Prepare()
        {
            Console.WriteLine("prepare ChicagoChickenPizza pizza");
        }
    }
}
