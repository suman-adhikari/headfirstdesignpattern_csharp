﻿using System;

namespace FactoryMethodApproachOne
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Factory Method");          
            PizzaStore store = new PizzaStore(new NYPizzaFactory());
            store.OrderPizza("mushroom");

            PizzaStore store1 = new PizzaStore(new ChicagoPizzaFactory());
            store1.OrderPizza("chicken");
        }
    }
}
