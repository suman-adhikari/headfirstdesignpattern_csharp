﻿using System;

namespace Command
{
    //Invoker
    public class Remote
    {
        private const int RemoteSize = 9;
        private ICommand[] onCommand = new ICommand[RemoteSize];
        private ICommand[] offCommand = new ICommand[RemoteSize];
        private ICommand noCommand = new NoCommand();
        private ICommand currentCommand = null;

        //by default initialize all command(on/off) with noCommand
        //so that calling remote ButtonClick with valid slot number gives no error;
        public Remote()
        {
            for(int i=0;i< RemoteSize; i++)
            {
                onCommand[i] = noCommand;
                offCommand[i] = noCommand;
            }
        }

        public void SetCommand(int slot, ICommand onCommand, ICommand offCommand)
        {
            this.onCommand[slot] = onCommand;
            this.offCommand[slot] = offCommand;
        }
    
        public void DisplayRemoteControl()
        {
            Console.WriteLine("Remote control");
            for (int i=0; i< RemoteSize; i++)
            {            
                Console.WriteLine($"slot {i}: {this.onCommand[i].GetType().Name}   {this.offCommand[i].GetType().Name}");
            }
        }


        public void ButtonClick(int slot, bool on=false)
        {
            
            if (on)
            {
                onCommand[slot].execute();
                currentCommand = onCommand[slot];

            }
            else
            {
                offCommand[slot].execute();
                currentCommand = offCommand[slot];
            }
        }

        public void UndoClick()
        {
            currentCommand.undo();
        }
    }
}
