﻿using System;
using static Command.Common;

namespace Command
{
    public interface IReceiver
    {
        public void On();

        public void Off();
    }

    public interface ILight : IReceiver
    {
        public void Dim();
    }

    public class PartyLight : ILight
    {
        public void Dim()
        {
            Console.WriteLine("PartyLight dim");
        }

        public void Off()
        {
            Console.WriteLine("PartyLight off");
        }

        public void On()
        {
            Console.WriteLine("PartyLight on");
        }
    }

    public class DinnerLight : ILight
    {
        public void Dim()
        {
            Console.WriteLine("DinnerLight dim");
        }

        public void Off()
        {
            Console.WriteLine("DinnerLight off");
        }

        public void On()
        {
            Console.WriteLine("DinnerLight on");
        }
    }

   
    public interface IFan : IReceiver
    {
        public void High();
        public void Medium();
        public void Low();

        public SPEED GetSpeed();
    }

    public class Common
    {
        public enum SPEED { Low, Medium, High }
    }
    public class TableFan : IFan
    {
        SPEED Speed = SPEED.Low;

        public void High()
        {
            Speed = SPEED.High;
            Console.WriteLine("TableFan High speed");
        }

        public void Low()
        {
            Speed = SPEED.Low;
            Console.WriteLine("TableFan Low speed");
        }

        public void Medium()
        {
            Speed = SPEED.Medium;
            Console.WriteLine("TableFan Medium speed");
        }

        public void Off()
        {
            Console.WriteLine("TableFan off");
        }

        public void On()
        {
            Console.WriteLine("TableFan on");
        }

        public SPEED GetSpeed()
        {
            return Speed;
        }
        
    }


    public class CeilingFan : IFan
    {
        SPEED Speed = SPEED.Low;
        public void High()
        {
            Speed = SPEED.High;
            Console.WriteLine("CeilingFan High speed");
        }

        public void Low()
        {
            Speed = SPEED.Low;
            Console.WriteLine("CeilingFan Low speed");
        }

        public void Medium()
        {
            Speed = SPEED.Medium;
            Console.WriteLine("CeilingFan Medium speed");
        }

        public void Off()
        {
            Console.WriteLine("CeilingFan off");
        }

        public void On()
        {
            Console.WriteLine("CeilingFan on");
        }

        public SPEED GetSpeed()
        {
            return Speed;
        }
    }

    public interface Iplayer : IReceiver
    {
        public void InsertDevice(string device);//could be VCR,CD,DVD,USB

        public void EjectDevice(string device);

        public void VolumeUp(int volumn);
        public void VolumeDown(int volumn);
    }

    public class CDPlayer : Iplayer
    {
        public void InsertDevice(string device)
        {
            Console.WriteLine($"inserting {device}");
        }

        public void EjectDevice(string device)
        {
            Console.WriteLine($"ejecting {device}");
        }

        public void Off()
        {
            Console.WriteLine("CDPlayer off");
        }

        public void On()
        {
            Console.WriteLine("CDPlayer on");
        }

        public void VolumeUp(int volumn)
        {
            Console.WriteLine($"increase volumn {volumn}");
        }

        public void VolumeDown(int volumn)
        {
            Console.WriteLine($"decreasing volumn {volumn}");
        }
    }

}
