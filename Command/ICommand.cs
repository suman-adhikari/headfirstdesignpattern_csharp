﻿using System;
using static Command.Common;

namespace Command
{
    public interface ICommand
    {
        public void execute();

        public void undo();
    }

    public class NoCommand : ICommand
    {
        public void execute()
        {
            Console.WriteLine("This slot is not allocated");
        }

        public void undo()
        {
            Console.WriteLine("NO command");
        }
    }
    public class LightOnCommand : ICommand
    {
        ILight light;

        public LightOnCommand(ILight light)
        {
            this.light = light;
        }
        public void execute()
        {
            light.On();
        }

        public void undo()
        {
            light.Off();
        }
    }

    public class LightOffCommand : ICommand
    {
        ILight light;

        public LightOffCommand(ILight light)
        {
            this.light = light;
        }
        public void execute()
        {
            light.Off();
        }

        public void undo()
        {
            light.On();
        }
    }

    public class LightDimCommand : ICommand
    {
        ILight light;

        public LightDimCommand(ILight light)
        {
            this.light = light;
        }
        public void execute()
        {
            light.Dim();
        }

        public void undo()
        {
            throw new NotImplementedException();
        }
    }

    public class PlayerOnCommand : ICommand
    {
        Iplayer player;

        public PlayerOnCommand(Iplayer player)
        {
            this.player = player;
        }
        public void execute()
        {
            player.On();
        }

        public void undo()
        {
            player.Off();
        }
    }

    public class PlayerOffCommand : ICommand
    {
        Iplayer player;

        public PlayerOffCommand(Iplayer player)
        {
            this.player = player;
        }
        public void execute()
        {
            player.Off();
        }

        public void undo()
        {
            player.On();
        }
    }

    public class PlayerInsertCDCommand : ICommand
    {
        Iplayer player;

        public PlayerInsertCDCommand(Iplayer player)
        {
            this.player = player;
        }
        public void execute()
        {
            player.InsertDevice("CD");
        }

        public void undo()
        {
            player.EjectDevice("CD");
        }
    }

    public class PlayerEjectCDCommand : ICommand
    {
        Iplayer player;

        public PlayerEjectCDCommand(Iplayer player)
        {
            this.player = player;
        }
        public void execute()
        {
            player.EjectDevice("CD");
        }

        public void undo()
        {
            player.InsertDevice("CD");
        }
    }

    public class PlayerVolumeUpCommand : ICommand
    {
        Iplayer player;

        public PlayerVolumeUpCommand(Iplayer player)
        {
            this.player = player;
        }
        public void execute()
        {
            player.VolumeUp(12);
        }

        public void undo()
        {
            player.VolumeDown(12);
        }
    }

    public class PlayerVolumeDownCommand : ICommand
    {
        Iplayer player;

        public PlayerVolumeDownCommand(Iplayer player)
        {
            this.player = player;
        }
        public void execute()
        {
            player.VolumeDown(11);
        }

        public void undo()
        {
            player.VolumeUp(12);
        }
    }

    public class FanOnCommand : ICommand
    {
        IFan fan;

        public FanOnCommand(IFan fan)
        {
            this.fan = fan;
        }

        public void execute()
        {
            fan.On();
        }

        public void undo()
        {
            fan.Off();
        }
    }

    public class FanOffCommand : ICommand
    {
        IFan fan;

        public FanOffCommand(IFan fan)
        {
            this.fan = fan;
        }

        public void execute()
        {
            fan.Off();
            
        }

        public void undo()
        {
            fan.On();
        }
    }

    public class FanMediumCommand : ICommand
    {
        IFan fan;
        SPEED prevSpeed;
       
        public FanMediumCommand(IFan fan)
        {
            this.fan = fan;
        }

        public void execute()
        {
            prevSpeed = fan.GetSpeed();
            fan.Medium();
        }

        public void undo()
        { 
            if (prevSpeed == SPEED.High)
                fan.High();
            else if (prevSpeed == SPEED.Medium)
                fan.Medium();
            else
                fan.Low();

        }
    }

    public class FanHighCommand : ICommand
    {
        IFan fan;
        SPEED prevSpeed;
        public FanHighCommand(IFan fan)
        {
            this.fan = fan;
        }

        public void execute()
        {
            prevSpeed = fan.GetSpeed();
            fan.High();
        }

        public void undo()
        {
            if (prevSpeed == SPEED.High)
                fan.High();
            else if (prevSpeed == SPEED.Medium)
                fan.Medium();
            else
                fan.Low();

        }
    }

    public class FanLowCommand : ICommand
    {
        IFan fan;
        SPEED prevSpeed;

        public FanLowCommand(IFan fan)
        {
            this.fan = fan;
        }

        public void execute()
        {
            prevSpeed = fan.GetSpeed();
            fan.Low();
        }

        public void undo()
        {
            if (prevSpeed == SPEED.High)
                fan.High();
            else if (prevSpeed == SPEED.Medium)
                fan.Medium();
            else
                fan.Low();

        }
    }

    public class PartyModeOnCommand :ICommand
    {
        ICommand[] command;
        public PartyModeOnCommand(ICommand[] command)
        {
            this.command = command;
        }
        public void execute()
        {
            for(int i = 0; i < command.Length; i++)
            {
                command[i].execute();
            }
        }

        public void undo()
        {

            for (int i = 0; i < command.Length; i++)
            {
                command[i].undo();
            }
        }
    }

}
