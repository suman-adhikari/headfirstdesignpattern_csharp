﻿using System;

namespace Command
{
    class Program
    {
        //This implement a remote that have different slot for different command.
        //Each slot is mapped with one command, once slot is set, you can call any command set in the various slot.
        static void Main(string[] args)
        {
            Console.WriteLine("Command Pattern");

            Remote remote = new Remote();
            PartyLight plight = new PartyLight();
           
            LightOnCommand lighton = new LightOnCommand(plight);
            LightOffCommand lightoff = new LightOffCommand(plight);

            int LightSlot = 0;
            int PlayerSlot = 1;
            int FanSlot = 2;
            int VolumnSlot = 3;
            int InsertEjectDevice = 4;
            int FanHigh = 5;
            int FanMedium = 6;
            int FanLow = 7;
            int PartyMode = 8;

            //Here on ButtonClick() the remote will perform Light On operation,
            //but the remote class has no idea, what operaion will be performed when button is clicked
            //This is method encapsulation
            //The remote will receive a command from SetCommand(),
            //the command know what operation to perform on what receiver
            //The invoker(remote) is decoupled from the receiver(Light,Fan,Player) that actually performs certain action.
            remote.SetCommand(LightSlot, lighton, lightoff);        
         

            CDPlayer cdplayer = new CDPlayer();
            PlayerOnCommand playerOn = new PlayerOnCommand(cdplayer);
            PlayerInsertCDCommand playerInsertDevice = new PlayerInsertCDCommand(cdplayer);
            PlayerEjectCDCommand playerEjectDevice = new PlayerEjectCDCommand(cdplayer);
            PlayerVolumeUpCommand volumeUp = new PlayerVolumeUpCommand(cdplayer);
            PlayerVolumeDownCommand volumeDown = new PlayerVolumeDownCommand(cdplayer);
            PlayerOffCommand playeroff = new PlayerOffCommand(cdplayer);

            remote.SetCommand(PlayerSlot, playerOn, playeroff);
            remote.SetCommand(InsertEjectDevice, playerInsertDevice, playerEjectDevice);
            remote.SetCommand(InsertEjectDevice, playerInsertDevice, playerEjectDevice);
            remote.SetCommand(VolumnSlot, volumeUp, volumeDown);

            //FAN : Undo for FAN is not as straightforward as previous example.
            // Here we need to maintain state
            TableFan tfan = new TableFan(); // If you change TableFan to CelingFan here, all operatio will be performed on CeilingFan 
            FanOnCommand onFan = new FanOnCommand(tfan);
            FanOffCommand offFan = new FanOffCommand(tfan);
            FanHighCommand highFan = new FanHighCommand(tfan);
            FanMediumCommand MediumFan = new FanMediumCommand(tfan);
            FanLowCommand lowFan = new FanLowCommand(tfan);

            remote.SetCommand(FanSlot, onFan, offFan);
            remote.SetCommand(FanHigh, highFan, offFan);
            remote.SetCommand(FanMedium, MediumFan, offFan);
            remote.SetCommand(FanLow, lowFan, offFan);

            //party mode: use this when you want macro operation ie run multiple operation at once
            ICommand[] Oncmd = new ICommand[] { lighton, playerOn, playerInsertDevice, volumeUp, onFan };
            ICommand[] Offcmd = new ICommand[] { playeroff, offFan, lightoff};
            PartyModeOnCommand partyOn = new PartyModeOnCommand(Oncmd);
            PartyModeOnCommand partyOff = new PartyModeOnCommand(Offcmd);
            remote.SetCommand(PartyMode, partyOn, partyOff);

            remote.DisplayRemoteControl();

            //operation performed: light on, cdplayer on, insert cd, voulme up, light off, light on, volumn dowm , volumn up
            remote.ButtonClick(LightSlot,true);
            remote.ButtonClick(PlayerSlot,true);
            remote.ButtonClick(InsertEjectDevice, true);
            remote.ButtonClick(VolumnSlot, true);
            remote.ButtonClick(LightSlot, false);
            remote.UndoClick(); // this will undo previous command
            remote.ButtonClick(VolumnSlot, false);
            remote.UndoClick(); 
            remote.ButtonClick(FanSlot);

            //fan operation
            remote.ButtonClick(FanSlot, true);
            remote.ButtonClick(FanHigh, true);
            remote.ButtonClick(FanMedium, true);
            remote.UndoClick();
            remote.ButtonClick(FanLow, true);
            remote.ButtonClick(FanHigh, true);
            remote.UndoClick();

            //party mode: use this when you want macro operation ie run multiple operation at once
            Console.WriteLine("\n ### PARTY MODE ON ### ");
            remote.ButtonClick(PartyMode, true);
            Console.WriteLine("\n ### PARTY MODE UNDO  ### ");
            remote.UndoClick();
            //Console.WriteLine("\n ### PARTY MODE OFF ### ");
            //remote.ButtonClick(PartyMode, false);


        }
    }
}
