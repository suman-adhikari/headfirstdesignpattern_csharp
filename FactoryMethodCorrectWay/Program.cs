﻿using System;

namespace FactoryMethodCorrectWay
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Factory Method correct way");

            PizzaStore nyPizzaStore = new NYPizzaStore();
            nyPizzaStore.OrderPizza("cheese");

            PizzaStore chicagoPizzaStore = new ChicagoPizzaStore();
            chicagoPizzaStore.OrderPizza("mushroom");
        }
    }
}
