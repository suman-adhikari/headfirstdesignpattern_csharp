﻿
namespace FactoryMethodCorrectWay
{
    //abstract creator or abstract factory
    public abstract class PizzaStore
    {
        public Pizza OrderPizza(string type)
        {
            Pizza pizza = CreatePizza(type);
            pizza.Prepare();
            pizza.Bake();
            pizza.Cut();
            pizza.Box();
            return pizza;
        }

        //abstract factorymethod
        public abstract Pizza CreatePizza(string type);
    }

    // concrete creators
    public class NYPizzaStore : PizzaStore
    {
        Pizza pizza;

        //concrete factory method
        public override Pizza CreatePizza(string type)
        {
            if (type == "cheese")
                pizza = new NYCheesePizza();
            else if (type == "chicken")
                pizza = new NYChickenPizza();
            else if (type == "cheese")
                pizza = new NYMushroomPizza();
            else
                pizza = null;

            return pizza;
        }
    }

    public class ChicagoPizzaStore : PizzaStore
    {
        Pizza pizza;
        public override Pizza CreatePizza(string type)
        {
            if (type == "cheese")
                pizza = new ChicagoCheesePizza();
            else if (type == "chicken")
                pizza = new ChicagoChickenPizza();
            else if (type == "mushroom")
                pizza = new ChicagoMushroomPizza();
            else
                pizza = null;

            return pizza;
        }
    }


}
