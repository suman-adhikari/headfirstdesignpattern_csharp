﻿using System;

namespace FactoryMethodCorrectWay
{
    //Product
    public abstract class Pizza
    {
        public string dough;
        public abstract void Prepare();

        public void Bake()
        {
            Console.WriteLine("baking pizza");
        }

        public void Cut()
        {
            Console.WriteLine("cutting pizza");
        }

        public void Box()
        {
            Console.WriteLine("boxing pizza");
        }
    }
    //NewYork and Chicago might have different way of preparing pizza based on local customer's choise
    //NewYorkers love thin crust with little sause and cheese
    //Chicago people love tick crust with heavy sause and cheese
    //But the fracnchise wants the way to bake,cut and box(packaging box) to be thes same to keep consistency.
    

    //concrete products
    public class NYCheesePizza : Pizza
    {
        public NYCheesePizza()
        {
            dough = "Ny style thin crust";
        }
        public override void Prepare()
        {
            Console.WriteLine("Preparing NYCheesePizza pizza");
            Console.WriteLine($"dough has {dough}");
        }
    }

    public class ChicagoCheesePizza : Pizza
    {
        public ChicagoCheesePizza()
        {
            dough = "Chicago style thick crust";
        }
        public override void Prepare()
        {
            Console.WriteLine("Preparing ChicagoCheesePizza pizza");
            Console.WriteLine($"dough has {dough}");
        }
    }

    public class NYMushroomPizza : Pizza
    {
        public NYMushroomPizza()
        {
            dough = "Ny style thin crust";
        }
        public override void Prepare()
        {
            Console.WriteLine("Preparing NYMushroomPizza pizza");
            Console.WriteLine($"dough has {dough}");
        }
    }

    public class ChicagoMushroomPizza : Pizza
    {
        public ChicagoMushroomPizza()
        {
            dough = "Chicago style thick crust";
        }
        public override void Prepare()
        {
            Console.WriteLine("Preparing ChicagoMushroomPizza pizza");
            Console.WriteLine($"dough has {dough}");
        }
    }
    public class NYChickenPizza : Pizza
    {
        public NYChickenPizza()
        {
            dough = "Ny style thin crust";
        }
        public override void Prepare()
        {
            Console.WriteLine("Preparing NYChickenPizza pizza");
            Console.WriteLine($"dough has {dough}");
        }
    }

    public class ChicagoChickenPizza : Pizza
    {

        public ChicagoChickenPizza()
        {
            dough = "Chicago style thick crust";
        }
        public override void Prepare()
        {
            Console.WriteLine("Preparing ChicagoChickenPizza pizza\n");
            Console.WriteLine($"dough has {dough}");
        }
    }
}
