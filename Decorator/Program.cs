﻿using System;

namespace Decorator
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Decorator Pattern");
           
            Beverage darkRoast = new DarkRoastedCoffee();
            darkRoast.Size = Beverage.SIZE.Medium;
            Beverage milkDrakRoast = new Milk(darkRoast);
            double price = milkDrakRoast.Cost();
            string desc = milkDrakRoast.Description();
            Console.WriteLine($"Beverage: {desc}  cost: {price}");

            Beverage DrakRoast = new DarkRoastedCoffee();
            DrakRoast.Size = Beverage.SIZE.Small;
            Beverage creamDrakRoast = new Cream(DrakRoast);
            double price1 = creamDrakRoast.Cost();
            string desc1 = creamDrakRoast.Description();
            Console.WriteLine($"Beverage: {desc1} cost: {price1}");

            Beverage expresso = new Expresso();
            expresso.Size = Beverage.SIZE.Large;
            Beverage milkExpresso = new Milk(expresso);
            Beverage doubleMilkExpresdsot = new Milk(milkExpresso);
            double priceExpresso = doubleMilkExpresdsot.Cost();
            string descExpresso = doubleMilkExpresdsot.Description();
            Console.WriteLine($"Beverage: {descExpresso}  cost: {priceExpresso}");


        }
    }
}
 