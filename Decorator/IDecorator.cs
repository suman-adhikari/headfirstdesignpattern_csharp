﻿
namespace Decorator
{
    public abstract class DecoratorType : Beverage
    {
        public Beverage _beverage;
    }

    public class Cream : DecoratorType
    {
       
        private double Large = 0.5;
        private double Medium = 0.4;
        private double Small = 0.3;
        public Cream(Beverage beverage)
        {
            _beverage = beverage;
        }
        
        public override double Cost()
        {
            double cost = _beverage.Cost();
            switch (_beverage.Size)
            {
                case SIZE.Large:
                    return cost + Large;
                case SIZE.Medium:
                    return cost + Medium;
                default:
                    return cost + Small;
            }
        }

        public override string Description()
        {
            return $"{_beverage.Description()} {this.GetType().Name}";
        }
    }

    public class Milk : DecoratorType
    {
        private double Large = 0.5;
        private double Medium = 0.4;
        private double Small = 0.3;
        public Milk(Beverage beverage)
        {
            _beverage = beverage;
        }
        public override double Cost()
        {
            double cost = _beverage.Cost();
            switch (_beverage.Size)
            {
                case SIZE.Large:
                    return cost + Large;
                case SIZE.Medium:
                    return cost + Medium;
                default:
                    return cost + Small;
            }
        }

        public override string Description()
        {
            return $"{_beverage.Description()} {this.GetType().Name}";
        }
    }
}
