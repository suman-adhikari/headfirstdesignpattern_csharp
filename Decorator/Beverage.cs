﻿
namespace Decorator
{
    //Component
    public abstract class Beverage
    {
        public enum SIZE{ Large, Medium, Small};
        public abstract string Description();
        public abstract double Cost();
        public SIZE Size { get; set; }
      
    }

    //concreate components

    public class DarkRoastedCoffee : Beverage
    {
        private double Large = 3.6;
        private double Medium = 3.5;
        private double Small = 3.4;
        public override string Description()
        {
            return $"{Size} DarkRoastedCoffee";
        }
        public override double Cost()
        {
            switch (Size)
            {
                case SIZE.Large:
                    return Large;
                case SIZE.Medium:
                    return Medium;
                default:
                    return Small;
            }
        }
    }

    public class Decaf : Beverage
    {
        private double Large = 4.0;
        private double Medium = 3.7;
        private double Small = 3.5;
        public override string Description()
        {
            return $"{Size} Decaf";
        }
        public override double Cost()
        {
            switch (Size)
            {
                case SIZE.Large:
                    return Large;
                case SIZE.Medium:
                    return Medium;
                default:
                    return Small;
            }
        }
    }

    public class Expresso : Beverage
    {
        private double Large = 4.5;
        private double Medium = 4.0;
        private double Small = 3.5;
        public override string Description()
        {
            return $"{Size} Expresso";
        }
        public override double Cost()
        {
            switch (Size)
            {
                case SIZE.Large:
                    return Large;
                case SIZE.Medium:
                    return Medium;
                default:
                    return Small;
            }
        }
    }


}
