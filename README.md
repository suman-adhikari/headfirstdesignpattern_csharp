#Factory Method

The factory method defines a an interface for creating object but let's subclass decide which class to instantiate.

It has 4 main component:

1.  abstract Product(pizza) - so that we can use it instead of concrete product to do various operation on it.

2. ConcreteProduct(NyCheesePizza,ChicagoCheesepizza) - actual product which inherits the product

3. abstract Creator(pizzaStore) - It is a class that contains the implementation for all the methods to manipulate product except for the factorymethod()

4. ConcreteCreator(NYPizzaStore,ChicagoPizzaStore) - It inherits the creator and gives the implementation for abstract factorymethod() inherited from creator. This implementation creates the concrete product.


The abstract creator gives you an interface with an abstract method for creating objects (factorymethod). The abstract method is implemented in the subclass to create concrete product. Any other ,ethod of abstract creator are written to operate on product produced by factorymethod().



Awesome links:
Difference between simple factory and factory method
https://dzone.com/articles/factory-method-vs-simple-factory-1
