﻿using System;

namespace Adapter
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Adapter Pattern");

            MallardDuck duck = new MallardDuck();
            BronzeTurkey bronzeTurkey = new BronzeTurkey();

            TurkeyAdaptor adapter = new TurkeyAdaptor(bronzeTurkey);// wrapping object

            //passing actual duck to clinet
            TestDuck(duck);

            //passing turkey wrapped in duck
            TestDuck(adapter);

        }

        //Client : accept only DUCK interface
        //TestDuck accepts only the Duck type
        //Here we are also passing turkey wrapped as duck and client has no idea of it. For client it seems to be a DUCK.
        //The client gives expected result when adaptor is passed to it and never knows there is an adapter doing the translation.

        //NOTE: the client and the adaptee(Iturkey) are decoupled and neither has knowledge about another.
        public static void TestDuck(IDuck duck)
        {
            duck.quack();
            duck.fly();
        }
    }
}
