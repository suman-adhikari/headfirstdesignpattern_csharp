﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Adapter
{
    //The adapter always 1. implement the target interfae and 2. compose the adaptee interface
    public class TurkeyAdaptor : IDuck
    {
        private ITurkey turkey;

        public TurkeyAdaptor(ITurkey turkey)
        {
            this.turkey = turkey;
        }
        public void fly()
        {
            turkey.shortfly();
        }

        public void quack()
        {
            turkey.gobble();
        }
    }

   
}
