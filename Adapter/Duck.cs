﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Adapter
{
    //Target Interface
    public interface IDuck
    {
        public void quack();

        public void fly();
    }

    public class MallardDuck : IDuck
    {
        public void fly()
        {
            Console.WriteLine($"{GetType().Name} fly");
        }

        public void quack()
        {
            Console.WriteLine($"{GetType().Name} says quack");
        }
    }

    //Adaptee Interface, the interface that is not compatible
    // Our client accept only Duck interface, hence we need an adapter that will convert this interface to duck interface
    public interface ITurkey
    {
        public void gobble();

        public void shortfly();
    }

    public class BronzeTurkey : ITurkey
    {
        public void gobble()
        {
            Console.WriteLine($"{GetType().Name} says gobble");
        }

        public void shortfly()
        {
            Console.WriteLine($"{GetType().Name} fly short");
        }
    }
}
