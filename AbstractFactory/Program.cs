﻿using System;

namespace AbstractFactory
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Hello World!");
            PizzaStore nyPizzaStore = new NyPizzaStore();
            nyPizzaStore.OrderPizza("cheese");

            PizzaStore chicagoPizzaStore = new ChicagoPizzaStore();
            chicagoPizzaStore.OrderPizza("mushroom");
        }
    }
}
