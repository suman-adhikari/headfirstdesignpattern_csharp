﻿using System;
using System.Collections.Generic;
using System.Text;

namespace AbstractFactory
{
    public interface IPizzaIngredientFactory
    {
        public IDough createDough();
        public ISauce createSauce();
        public ICheese createCheese();
        public Veggies[] createVeggies();
    }

    public class NyPizzaIngredientFactory : IPizzaIngredientFactory
    {
        public ICheese createCheese()
        {
            return new ReggianoCheese();
        }

        public IDough createDough()
        {
            return new ThinCrustDough();
        }

        public ISauce createSauce()
        {
            return new ChillySauce();
        }

        public Veggies[] createVeggies()
        {
            return new Veggies[] { new Tomato(), new RedPepper() };
        }
    }

    public class ChicagoPizzaIngredientFactory : IPizzaIngredientFactory
    {
        public ICheese createCheese()
        {
            return new YakCheese();
        }

        public IDough createDough()
        {
            return new ThickCrustDough();
        }

        public ISauce createSauce()
        {
            return new SweetSauce();
        }

        public Veggies[] createVeggies()
        {
            return new Veggies[] { new Capcium(), new Onion() };
        }
    }
}
