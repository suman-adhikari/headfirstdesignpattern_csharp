﻿using System;
using System.Collections.Generic;
using System.Text;

namespace AbstractFactory
{
    public abstract class Pizza
    {
        public string Name { get; set; }
        public IDough dough;
        public ICheese cheese;
        public ISauce sauce;
        public Veggies[] veggies;

        public abstract void Prepare();
       

        public void Bake()
        {
            Console.WriteLine("Baking pizza");
        }

        public void Cut()
        {
            Console.WriteLine("Cutting pizza");
        }

        public void Box()
        {
            Console.WriteLine("Boxing pizza");
        }

    }

    public class CheesePizza : Pizza
    {
        IPizzaIngredientFactory pizzaIngredientFactory;

        public CheesePizza(IPizzaIngredientFactory pizzaIngredientFactory)
        {
            this.pizzaIngredientFactory = pizzaIngredientFactory;
        }
        public override void Prepare()
        {
            Console.WriteLine($"Preparing {Name}");
            dough = pizzaIngredientFactory.createDough();
            cheese = pizzaIngredientFactory.createCheese();
            sauce = pizzaIngredientFactory.createSauce();
            veggies = pizzaIngredientFactory.createVeggies();
        }
    }

    public class MushroomPizza : Pizza
    {
        IPizzaIngredientFactory pizzaIngredientFactory;

        public MushroomPizza(IPizzaIngredientFactory pizzaIngredientFactory)
        {
            this.pizzaIngredientFactory = pizzaIngredientFactory;
        }
        public override void Prepare()
        {
            Console.WriteLine($"Preparing {Name}");
            dough = pizzaIngredientFactory.createDough();
            cheese = pizzaIngredientFactory.createCheese();
            sauce = pizzaIngredientFactory.createSauce();
            veggies = pizzaIngredientFactory.createVeggies();
        }
    }

}
