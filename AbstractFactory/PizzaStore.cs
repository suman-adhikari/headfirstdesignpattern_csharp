﻿using System;
using System.Collections.Generic;
using System.Text;

namespace AbstractFactory
{
    public abstract class PizzaStore
    {
        public Pizza OrderPizza(string type)
        {
            Pizza pizza = CreatePizza(type);
            pizza.Prepare();
            pizza.Bake();
            pizza.Cut();
            pizza.Box();
            return pizza;
        }

        public abstract Pizza CreatePizza(string type); 
    }

    public class NyPizzaStore : PizzaStore
    {
        NyPizzaIngredientFactory nyPizzaIngredientFactory = new NyPizzaIngredientFactory();

        public override Pizza CreatePizza(string type)
        {
            Pizza pizza;
            if (type == "cheese")
            {
                pizza = new CheesePizza(nyPizzaIngredientFactory);
                pizza.Name = "NY Cheese pizza";
            }
            else if (type == "mushroom")
            {
                pizza = new MushroomPizza(nyPizzaIngredientFactory);
                pizza.Name = "NY Mushroom pizza";
            }
            else
                pizza = null;
            return pizza;
        }
    }

    public class ChicagoPizzaStore : PizzaStore
    {
        ChicagoPizzaIngredientFactory chicagoPizzaIngredientFactory = new ChicagoPizzaIngredientFactory();

        public override Pizza CreatePizza(string type)
        {
            Pizza pizza;
            if (type == "cheese")
            {
                pizza = new CheesePizza(chicagoPizzaIngredientFactory);
                pizza.Name = "Chicago Cheese pizza";
            }
            else if (type == "mushroom")
            {
                pizza = new MushroomPizza(chicagoPizzaIngredientFactory);
                pizza.Name = "Chicago Mushroom pizza";
            }
            else
                pizza = null;
            return pizza;
        }
    }
}
