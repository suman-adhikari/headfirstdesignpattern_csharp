﻿using System;
using System.Collections.Generic;
using System.Text;

namespace AbstractFactory
{
    public interface IDough
    {
       
    }
    public class ThinCrustDough : IDough
    {
       
    }

    public class ThickCrustDough : IDough
    {

    }

    public interface ISauce
    {

    }

    public class ChillySauce : ISauce
    {

    }

    public class SweetSauce : ISauce
    {

    }

    public class CreamySauce : ISauce
    {

    }

    public interface ICheese
    {

    }

    public class YakCheese : ICheese
    {

    }

    public class ReggianoCheese : ICheese
    {

    }

    public abstract class Veggies
    {

    }

    public class Onion : Veggies
    {

    }

    public class Capcium : Veggies
    {

    }

    public class Tomato : Veggies
    {

    }

    public class RedPepper : Veggies
    {

    }
}
