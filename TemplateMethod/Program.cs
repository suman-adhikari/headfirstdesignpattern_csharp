﻿using System;

namespace TemplateMethod
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("--Template Method Pattern--");

            Console.WriteLine("\npreparing Tea");
            Tea tea = new Tea();
            tea.PrepareReceipe(); 

            Console.WriteLine("\npreparing coffee");
            Coffee coffee = new Coffee();
            coffee.PrepareReceipe();

        }
    }
}
