﻿using System;

namespace TemplateMethod
{
    public class Template
    {
        public static string AskCustomer()
        {
            Console.WriteLine("Do you want to add lemon to tea?Y/N :");
            string choice = Console.ReadLine();
            return choice;
        }
    }

    public abstract class Beverage
    {
        //This is the template method, that defines the step of an algorithm. You cannot modify the step of an algorithm, it's fixed
        //This method is public, so that any implementation of this abstract class can call it.
        //THe 4th step of the algorithm is dependent on the user choice.
        //hook can be used to conditionally control the flow of the algorithm in the abstract class
        public void PrepareReceipe()
        {
            BoilWater();
            Brew();
            PourInCup();
            if (WantCondiments())
            {
                AddCondiments();
            }
        }

        //these two methods are common on both beverage and hence defined here   
        public void BoilWater()=>Console.WriteLine("Boiling water");
        public void PourInCup() => Console.WriteLine("pouring into cup");

        //These two methods will have different implementation for each class hence made abstract
        protected abstract void Brew();
        protected abstract void AddCondiments();

        //This is a hook method, that has default implementation that sublass can override but don't have to.
        //It can bring alteration to the steps of an algorithm
        //We define it virtual so that it can be overridden
        protected virtual bool WantCondiments()
        {
            return true;
        }

    }

    public class Tea : Beverage
    {
        protected override bool WantCondiments()
        {
            //we don't want to repet the same code in subclass, so a seperate static function is created to get user input
            string choice = Template.AskCustomer();
            return choice.ToLower() == "y" ? true : false;
        }
        protected override void Brew()
        {
            Console.WriteLine("steeping teabag in hot water");
        }
        protected override void AddCondiments()
        {
            Console.WriteLine($"Adding lemon to tea");
           
        }  
    }

    public class Coffee : Beverage
    {
        protected override bool WantCondiments()
        {
            string choice = Template.AskCustomer();
            return choice.ToLower() == "y" ? true : false;
        }
        protected override void Brew()
        {
            Console.WriteLine("adding coffee in hot water");
        }
        protected override void AddCondiments()
        {
            Console.WriteLine("Adding milk and sugar to coffee");
        }
    }
}
