﻿
namespace Singleton
{
   
    public class Singleton
    {
        public static Singleton instance;
        public static readonly object _lock = new object();

        private Singleton()
        {
                                                                                    
        }
        //Method1: This implementation of singleton is not threadsafe
        public static Singleton getInstance()
        {
            if (instance == null)
            {
                instance = new Singleton();
            }
            System.Console.WriteLine($"instance's hashcode: {instance.GetHashCode()}");
            return instance;
        }
        //other methods
    }


    public class ThreadSafeSingleton
    {
        public static ThreadSafeSingleton instance;
        public static readonly object _lock = new object();

        private ThreadSafeSingleton()
        {

        }

        //Method1: ThreadSafeSingleton with lock
        public static ThreadSafeSingleton getInstance()
        {
            //lock locks the current thread, execute the task and released once the exeution is completed.
            //IT ensures no other thread casn interrupt the execution until it is completed.
            //lock is expensive, everytime singleton instance is requested lock is encountered, it will be painful 
            //The solutio is to use double-checked lock to insure lock is encountered only in the first call to
            //get the instance
            lock (_lock)
            {
                if (instance == null)
                {
                    instance = new ThreadSafeSingleton();
                }
                System.Console.WriteLine($"instance's hashcode: {instance.GetHashCode()}");
                return instance;
            }
        }


        //Method2: ThreadSafeSingleton with double-checked lock
        public static ThreadSafeSingleton getInstance1()
        {
            //lock locks the current thread, execute the task and released once the exeution is completed.
            //IT ensures no other thread casn interrupt the execution until it is completed.
            //lock is expensive, everytime singleton instance is requested lock is encountered, it will be painful 
            //The solutio is to use double-checked lock to insure lock is encountered only in the first call to
            //get the instance
            if (instance == null)
            {
                lock (_lock)
                {
                    if (instance == null)
                    {
                        instance = new ThreadSafeSingleton();
                    }           
                }               
            }
            System.Console.WriteLine($"double-checkedinstance: instance's hashcode: {instance.GetHashCode()}");
            return instance;
        }

        //other methods
    }

    //Yet Another Approach
    //Threadsafe singleton with Early loading(load before instance is requested)
    //Downfall? Instance is created even if it is requested
    //When to use? If your application always creates and use an instance 
    //or if the creating of instance is not task heavy
    //how thread safe?Unique instance of singleton is created when the class is loaded 
    //before any thread access the instance variable

    public class EarlySingleton
    {
        public static EarlySingleton instance = new EarlySingleton();
        private EarlySingleton()
        {

        }
        public static EarlySingleton getInstance()
        {
            System.Console.WriteLine($"Early singleton Instance's hashcode: {instance.GetHashCode()}");
            return instance;
        }
        //other methods
    }

}
