﻿using System;
using System.Threading;

namespace Singleton
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Singleton");
            //TEST EACH SCENERIO SEPERATELY(comment other scenerio)

            //Note : gives error=> inaccessible due to its protection level Singleton
            //Singleton s = new Singleton();

            //Senerio 1
            //var x = Singleton.getInstance();
            //var y = Singleton.getInstance();
            
            //Scenerio 2: Testing thread safe
            //Console.WriteLine("Checking thread safe\n");                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                    
            //Thread process1 = new Thread(() => Singleton.getInstance());
            //Thread process2 = new Thread(() => Singleton.getInstance());
            //process1.Start();
            //process2.Start();

            //The hashcode is different for each thread, meaning two instance was created
            //The code is certainly not thread safe

            //Scenerio 3 : Testing ThreadSafe SIngleton
            //Console.WriteLine("Checking thread safe\n");
            //Thread p1 = new Thread(() => ThreadSafeSingleton.getInstance());
            //Thread p2 = new Thread(() => ThreadSafeSingleton.getInstance());
            //p1.Start();
            //p2.Start();

            //Now the hashcode is same for each thread, meaning the instance used by both thread was same
            //Hence it is thread safe.


            //Scenerio 4 : Testing ThreadSafe double-checked lock
            Console.WriteLine("Checking thread safe\n");
            Thread l1 = new Thread(() => ThreadSafeSingleton.getInstance1());
            Thread l2 = new Thread(() => ThreadSafeSingleton.getInstance1());
            l1.Start();
            l2.Start();

            //Now the hashcode is same for each thread, meaning the instance used by both thread was same
            //Hence it is thread safe.

            //Scenerio 4 : Testing ThreadSafe EarlySingleton
            Console.WriteLine("Checking thread safe\n");
            Thread e1 = new Thread(() => EarlySingleton.getInstance());
            Thread e2 = new Thread(() => EarlySingleton.getInstance());
            e1.Start();
            e2.Start();

        }
    }
}
